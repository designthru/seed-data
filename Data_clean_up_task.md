# Project

## Cleanup and convert product dataset from csv to json

## Description:
Clean up web scraped data. Write a python script that cleans CSV data, extract/split data, for 57K products separated into 17 category CSV files. The Script should have the capability to add columns, change the column name, extract/split word(s) from one column to another, combining columns as nested arrays, and convert the data to JSON format.

##	Deliverable: 

- #### 17 updated/cleaned CSV files

- #### 17 JSON Product files

- #### The Python script used for cleaning the csv file and converting the csv to json

### Skills: Python, CSV, JSON

## Task: Clean Up CSV

- Change column headers to the following values: 


### Header Cleanup:

| Current Header |      | New Header |
| ------------- | ---: |---|
| store      | -------------------------------------------------------------------> | stores |
| main category | -------------------------------------------------------------------> | categories |
| sub category | -------------------------------------------------------------------> | subcategories |
| name          | -------------------------------------------------------------------> | name |
| quantity      | -----------------------------------------------------------------------------------------------------------------------> |unit_weight|
| price         | -------------------------------------------------------------------> |price|
| images        | -------------------------------------------------------------------> |image|
| description   | -------------------------------------------------------------------> |description|
| scrapedurl (remove |  |**type**|
|              | Copy data from the **name** column however separate each word with a "-" between each word---> |**slug**|
|               |  |**unit_price**|
|               |      |quantiy|
|               | Copy  data from **price** column |sale_price|
|               |  |discount_percent|
|               |  |sales|
|               |      |views|
|               |              Extracted URL Images link from the image column | gallery          |
|               | Extracted Brand names from the name or desciption columns |brand|
|               | Extracted Nutrition values and names from the desciption column |nutrition|
|               | Extracted Ingredients from the desciption column |Ingredients|



##  Brand 

Create a Python script to extract the brand/company name from the "**name or description**" column and add it to the "**brand**" column. Use this data collection to query against for brand name identification [Grocery_Brands_Database.csv](https://bitbucket.org/designthru/supercart-data/src/master/xlsx/Grocery_Brands_Database.csv):

Example:

| **name**                      | **brand**    |
| ------------------------------------ | ------------ |
| Gerber 3rd Foods Banana Apple Strawberry with Lil' Bits Purees Fruit | Gerber |


## Nutrition

The Python script should include a function that extracts nutrition data from the **"description"** column and add it to the "**nutrition**" column. The nutrition values delineate the following data/field values: and are identifiable by a group of words separated by a comma after the word ingredients appear.

| Percent Daily Values are based on a 2,000 calorie diet |      |
| ------------------------------------------------------ | ---- |
| Serving Size                                           |      |
| Servings Per Container                                 |      |
| Amount Per Serving Calories                            |      |
| Daily Value Total Fat                                  |      |
| Saturated Fat                                          |      |
| Polyunsaturated Fat                                    |      |
| Monounsaturated Fat                                    |      |
| Cholesterol                                            |      |
| Sodium                                                 |      |
| Total Carbohydrate                                     |      |
| Dietary Fiber                                          |      |
| Sugars                                                 |      |
| Protein                                                |      |
|                                                        |      |


## Ingredients

Ingredients are within the description column, identifiable by a group of words separated by a comma after the name ingredients. There is a break after the last ingredient.

| **Ingredients** |
| -------------------- |
| item1                    |
| item2                    |



## Images

  - Extract and separated image URL's from the **image** column into the **gallery** column - Keep the first url in the **image** column and add the others into the **gallery** column 
    **Example:**

| **image**                      | **gallery**    |
| ------------------------------------ | ------------ |
| https://d2d8wwwkmhfcva.cloudfront.net/800x/main-image.jpg | "https://d2d8wwwkmhfcva.cloudfront.net/800x/main-image.jpg","https://d2d8wwwkmhfcva.cloudfront.net/800x/main-image.jpg","https://d2d8wwwkmhfcva.cloudfront.net/800x/main-image.jpg" |


## Category & Subcategory

  - Change the "main category" column header with "category" and change "sub category" to "subcategory"
    **Example:**

| **categories**                         | **subcategories**|
| ------------------------------------ | ------------ |
| Babies | Baby Food & Formula |

These two collections catagories are relational. A **product** has mutiple **categories** and a **category** can have multiple **subcategories**. I would like to keep the category and subcategory as seperate columns in the CSV file. 

When converting the CSV to JSON the subcategory should be a child of category see below. 
 **Example:**

  ```json
   "categories": [
            {
                "name": ["Babies"],
                "subcategory": ["Baby Food & Formula"]
            }
        ]
  ```



### Stores

Merge **store** data from the [store-data.json](https://bitbucket.org/designthru/supercart-data/src/master/json/store-data.json) file with the generated product json data. Include store **name** and **location** data into the **store** field in the generate product json filesfrom the previous task. Location should include city and zip_code.
  **Example:**

| **store** / Many to Many Relationship |
| ------------------------------------ |
|A **product** can be available at multiple **store**  locations  searchable by **zip_code** or *city* |




  ```json
   "store": [
            {
                "name": "Key Food",
                "locations": [
                    {		"city": "Astoria",
                        "zip_code": "11105"
                    },
                   	{		"city": "Brooklyn",
                        "zip_code": "11234"
                    },
                ]
            }
        ]
  ```





## Expected Json data structure for a Product

```json
{
        "name":"" ,
        "images": "",
        "description": "",
        "slug": "",
        "price": "",
        "sale_price": "",
        "unit_weight": "",
        "unit_price": "",
        "discount_percent": "",
        "sales": "",
        "quantity": "",
    		"ingredient": [""],
    		"nutrition": [
            {"Serving Size":"",
            "Servings Per Container":"",
            "Amount Per Serving Calories":"",    
            "Daily Value Total Fat":"",   
            "Saturated Fat": "",   
            "Polyunsaturated Fat": "",    
            "Monounsaturated Fat": "",   
            "Cholesterol":"",    
            "Sodium":"",    
            "Total Carbohydrate":"",    
            "Dietary Fiber": "",    
            "Sugars": "",    
            "Protein":""}
        ],
        "views": "",
        "gallery": [
           ""
        ],
        "categories": [
            {
                "name": "",
                "subcategory": ""
            }
        ],
        "brand": "",
        "store": [
            {
                "name": "",
                "locations": [
                    {
                        "city": "",
                       	"zip_code": ""
                    }
                ]
            }
        ]
    },

```